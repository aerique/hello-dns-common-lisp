;;;;- tauth.lisp
;;;;-
;;;; # Teachable Authoritative Server in Common Lisp
;;;;
;;;; Inspiration: <https://github.com/ahupowerdns/hello-dns/blob/master/tdns/tauth.cc>
;;;;
;;;; Usage:
;;;;
;;;; - `sbcl --noinform --load tauth.lisp --eval "(main :port 5300)"`  
;;;;   (`:port` defaults to 53 (DNS) for which one needs root permissions)

;;; ## Packages

(load "tdns.lisp")

;; Prevent compiler warnings about undefined functions.
(defun process-question (buffer) (declare (ignore buffer)))

;;; ## Globals

(defvar *n_queries* 0)


;;; ## Handlers

(defun udp-handler (buffer)
  (declare (type (simple-array (unsigned-byte 8) *) buffer))
  ;(format t "=== new dns message (~D bytes) ===~%~S~%" (length buffer) buffer)
  (process-question buffer))


;;; ## Functions

;; Move some of this stuff to UDP-HANDLER.  (Doing it inside RESOLVE for now
;; because I got some kind of threading error when using QUIT inside the
;; handler.)
(defun process-question (buffer)
  (format t "[~D] ~S~%" (incf *n_queries*) (parse-dns-message buffer))
  ;; Always send back CRAFTED.
  (let ((out (coerce crafted2 '(vector (unsigned-byte 8)))))
    (setf (elt out 0) (elt buffer 0)
          (elt out 1) (elt buffer 1))
    out))


;;; ## Main Program

;; As opposed to C/C++ `main` is just an arbitrary name here.
(defun main (&key (listen-address "127.0.0.1") (port 53))
  (format t "tauth listening on ~A:~D (UDP)...~%" listen-address port)
  (handler-case
      (usocket:socket-server listen-address port #'udp-handler nil
                             :protocol :datagram :max-buffer-size 1500)
    (sb-sys:interactive-interrupt (e) (user-interrupt e)))  ; FIXME SBCL dep
  (quit))


(defun compile-tauth ()
  (save-lisp-and-die "tauth" :toplevel #'main :executable t))

